//
//  InterfaceController.swift
//  SushiWatch Extension
//
//  Created by Jyothis Rajan on 2019-11-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {
   
    @IBOutlet weak var leftButton: WKInterfaceButton!
    @IBOutlet weak var rightButton: WKInterfaceButton!
    @IBOutlet weak var startButton: WKInterfaceButton!
    @IBOutlet weak var gameLabel: WKInterfaceLabel!
    var tapGesture = WKTapGestureRecognizer()
    
    var startButtonActionFlag=true
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //
    }
    
    // Function to receive DICTIONARY from the watch
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        // Output message to terminal
        print("WATCH: I received a message: \(message)")
        
        // Get the "name" key out of the dictionary
        // and show it in the label
        let time = message["time"] as! String
        let power = message["powerup"] as! String
      
        
        if(time=="nil"){
            print("no time")
            
        }else if(time=="stopLabel"){
            gameLabel.setHidden(true)
        }else if(time=="over"){
            gameLabel.setHidden(false)
            self.leftButton.setHidden(true)
            self.rightButton.setHidden(true)
            gameLabel.setText("GAME OVER!!")
        }
        else{
            gameLabel.setHidden(false)
            gameLabel.setText("Hurry up!! \(time) seconds!!")
        }
        
    if(power=="nil"){
            print("no power")
        }else if(power=="start"){
            startButton.setTitle("PowerUp!!!")
            //startButton.setBackgroundColor(.yellow)
            startButton.setHidden(false)
            startButtonActionFlag=false
        }else if(power=="stop"){
            startButton.setHidden(true)
            startButtonActionFlag=true
        }
      
        
        
    }
    
    

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        self.leftButton.setHidden(true)
        self.rightButton.setHidden(true)
        self.gameLabel.setHidden(true)
//        tapGesture = WKTapGestureRecognizer(target: self, action: #selector(InterfaceController.myviewTapped(_:)))
//        tapGesture.numberOfTapsRequired = 1
//        tapGesture.numberOfTapsRequired = 1
//        viewTap.addGestureRecognizer(tapGesture)
//        viewTap.isUserInteractionEnabled = true
        
        
    }
    
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        print("---WATCH APP LOADED")
        
        if (WCSession.isSupported() == true) {
            
            // create a communication session with the phone
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            print("WC NOT supported!")
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
   
    @IBAction func leftButtonClick() {
        
        print("WATCH: Pressed button")
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            
            let message = ["direction":"left","gameStatus":"nil"] as [String : Any]
            
            // send the message to the watch
            WCSession.default.sendMessage(message, replyHandler: nil)
            print("WATCH: Pressed button inside function")
        }
    }
    @IBAction func rightButtonClick() {
        print("WATCH: Pressed button")
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            
            let message = ["direction":"right","gameStatus":"nil"] as [String : Any]
            
            // send the message to the watch
            WCSession.default.sendMessage(message, replyHandler: nil)
            print("WATCH: Pressed button inside function")
        }
    }
    
    @IBAction func startButtonAction() {
        print("WATCH: Pressed button")
        if(startButtonActionFlag){
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            
            let message = ["direction":"nil","gameStatus":"start","power":"false"] as [String : Any]
            self.leftButton.setHidden(false)
            self.rightButton.setHidden(false)
            self.startButton.setHidden(true)
            
            // send the message to the watch
            WCSession.default.sendMessage(message, replyHandler: nil)
            print("WATCH: Pressed button inside function")
            }}else{
            if (WCSession.default.isReachable) {
                // construct the message you want to send
                // the message is in dictionary
                
                let message = ["direction":"nil","gameStatus":"nil","power":"true"] as [String : Any]
                self.startButton.setHidden(true)
                
                // send the message to the watch
                WCSession.default.sendMessage(message, replyHandler: nil)
                print("WATCH: Pressed button inside function")
            }
        }
        
    }
    }
